<?php
/**
 * Configuration File
 *
 * All your configuration crap goes here and in phinx.yml.
 * 
 * @package vgstation13-web
 */

ini_set('display_errors','On');
error_reporting(E_ALL | E_STRICT);
/*
ini_set('display_errors','1');
ini_set('xdebug.dump.POST','*');
ini_set('xdebug.dump.FILES','*');
*/

/*
set_error_handler('myErrorHandler');
function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting
        return;
    }

    switch ($errno) {
    case E_USER_ERROR:
        echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
        echo "  Fatal error on line $errline in file $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Aborting...<br />\n";
        exit(1);
        break;

    case E_USER_WARNING:
        echo "<b>WARNING</b> [$errno] $errstr<br />\n";
        break;

    case E_USER_NOTICE:
        echo "<b>NOTICE</b> [$errno] $errstr<br />\n";
        break;

    default:
        echo "Unknown error type: [$errno] $errstr<br />\n";
        break;
    }

    // Don't execute PHP internal error handler
    return true;
}
*/

/**
 * Web Root
 *
 * Specifies absolute path to /vg/station-web, without dangling slashes.
 */
define('WEB_ROOT','');

define('DB_TYPE','mysqli');// or postgres

/**
 * Database DSN
 * 
 * Determines how /vg/station-web will connect to the database.
 *
 * Format: {driver}://{username}:{urlencoded password}@{hostname}/{schema}[?persist]
 */
define('DB_DSN','mysqli://username:'.rawurlencode('password').'@server.hostname/schema?persist');

/**
 * How long a session exists
 */
define('COOKIE_LIFETIME',24*60*60);

/**
 * Width of thumbnails.
 */
define('THUMB_WIDTH',150);

/**
 * Images per page
 */
define('NUM_IMAGES_PER_PAGE',8*10);

/**
 * Name of the session-tracking cookie.
 */
define('SESSION_TOKEN', 'vgstation13_session');
define('SESSION_DOMAIN', 'your.site.here');
